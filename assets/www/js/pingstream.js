var BOSH_SERVICE = 'http://192.168.1.10:7070/http-bind/'
var connection = null;
var user = 'testuser1@chevelle';
var userpassword = 'password';

function notifyUser(msg) 
{
    $('#notifications').append('<p>');
    $('#notifications').append(msg);
    $('#notifications').append('</p>');

    if (msg.getAttribute('from') == "testuser@127.0.0.1/pingstream") {
    	var elems = msg.getElementsByTagName('body');
    	var body = elems[0];
    	$('#notifications').append(Strophe.getText(body));
    }
	return true;
}

function onConnect(status)
{

     if (status === Strophe.Status.CONNECTED) {
          $('#notifications').append('connected');
         } else if (status === Strophe.Status.DISCONNECTED) {
           $('#notifications').append('<p>disconnected</p>');
         } else if (status === Strophe.Status.CONNECTING) {
           $('#notifications').append('<p>connecting</p>');
         } else if (status === Strophe.Status.DISCONNECTING) {
           $('#notifications').append('<p>disconnecting</p>');
         } else if (status === Strophe.Status.AUTHENTICATING) {
           $('#notifications').append('<p>authenticating</p>');
         } else if (status === Strophe.Status.CONNFAIL) {
           $('#notifications').append('<p>connfail</p>');
         } else if (status === Strophe.Status.AUTHFAIL) {
           $('#notifications').append('<p>authfail</p>');
         }

	if (status == Strophe.Status.CONNECTED) {
		$('#notifications').append('<p class="welcome">Hello! Any new posts will appear below.</p>');
		connection.addHandler(notifyUser, null, 'message', null, null,  null);
		connection.send($pres().tree());
	}		
}

$(document).ready(function () {
    $('#notifications').html('<p class="welcome">Trying to connect to openfire.</p>');
    connection = new Strophe.Connection(BOSH_SERVICE);
    connection.connect(	user, userpassword, onConnect);
    });