    var restUri = 'http://chevelle:8080/callback/create';
    var callbackRestUri = 'http://ec2-54-212-78-255.us-west-2.compute.amazonaws.com:9095/callback/create';
    //var callbackRestUri = 'http://localhost:8080/callback/create';
    //var restUri = 'http://ngsdev.inin.com/mobilizerrest/interaction/create';
    var createChatRequest = '{"queuetype":"workgroup","interactiontype":"chat","queuename":"mobilizerchat", "subject":"testsubject", "telephone":"5551212"}';

    $(document).ready(function() {

        $('#startChatting').click(function(){

            $.ajax({
                type:"POST",
                url: restUri,
                data: createChatRequest,
                dataType: "json",
                contentType: "application/json;charset=utf-8",

                success: function(response){

                        //response needs to be stringified.
                        var stringifiedResponse = window.JSON.stringify(response);

                        console.log(stringifiedResponse);

                        var chat = $.parseJSON(stringifiedResponse);

                        var roomId =  chat.roomId;
                        var roomPassword = chat.roomPassword;
                        var server = chat.server;
                        var chatRoomName = chat.id;

                        window.location.href = "chat_candy.html?roomId=" + roomId + "&roomPassword=" + roomPassword + "&server=" + server + "&roomName=" + chatRoomName;

                },

                error: function(httpRequest, message, errorThrown){
                    console.log("error" + message);
                    alert("Failed to start a chat");
                }

            });
        });

        $('#createCallback').click(function(e){

           e.preventDefault();

           var callbackRequest = new Object();
           callbackRequest.targetType = "workgroup";
           callbackRequest.target = "mobilizerchat";
           callbackRequest.subject = $("#callbackSubject").val();
           callbackRequest.participant = new Object();
           callbackRequest.participant.name = $("#callbackUser").val();
           callbackRequest.participant.telephone =  $("#callbackNumber").val();
           callbackRequest.clientToken = "{ef19a8b9-8307-da9a-d9fd796faa7a}";
           callbackRequest.language = "en-US";

           var createCallbackRequest = JSON.stringify(callbackRequest);

           console.log(createCallbackRequest);

           console.log('requestUri' + callbackRestUri);

           $.ajax({
               type: "POST",
               url: callbackRestUri,
               data: createCallbackRequest,
               dataType: "json",
               crossDomain: true,
               timeout: 8000,
               contentType: "application/json",

               success: function(response){

                       //response needs to be stringified.
                       var stringifiedResponse = window.JSON.stringify(response);

                       console.log(stringifiedResponse);

                       var callback = $.parseJSON(stringifiedResponse);
                      
                      //yeh, I know this is really really really ugly...
                       alert("Callback created. " + callback.callback.callback.callbackID);
               },

               error: function(XMLHttpRequest, textStatus, errorThrown){

                   console.log('error ' + textStatus);
                   console.log('errorThrown ' + errorThrown);
                   console.log('responseText ' + XMLHttpRequest.responseText);

                   alert('Failed to create callback' + errorThrown);
               }
           });
        });
    });